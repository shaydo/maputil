# maputil

Go library providing methods for working with maps

[![GoDoc](https://pkg.go.dev/badge/gitlab.com/shaydo/maputil)](https://pkg.go.dev/gitlab.com/shaydo/maputil)

```go
	m := map[string]int{
		"one":   1,
		"two":   2,
		"three": 3,
	}
	keys := Keys(m)
    values := Values(m)
```
