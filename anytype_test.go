package maputil

import (
	"sort"
	"testing"
)

func TestIntKeysValues(t *testing.T) {
	m := map[int]int{
		1: 21,
		8: 22,
		3: 21,
	}
	keys := Keys(m)
	sort.Ints(keys)
	if len(keys) != 3 || keys[0] != 1 || keys[1] != 3 || keys[2] != 8 {
		t.Errorf("expected [1, 3, 8], got %+v", keys)
	}
	values := Values(m)
	sort.Ints(values)
	if len(values) != 3 || values[0] != 21 || values[1] != 21 || values[2] != 22 {
		t.Errorf("expected [21, 21, 22], got %+v", values)
	}
}

func TestStructKeys(t *testing.T) {
	type myStruct struct {
		name string
		age  int
	}
	m := map[myStruct]int{
		{name: "Bill", age: 20}: 20,
		{name: "Bob", age: 20}:  20,
		{name: "John", age: 20}: 20,
	}
	keys := Keys(m)
	sort.Slice(keys, func(i, j int) bool { return keys[i].name < keys[j].name })
	if len(keys) != 3 || keys[0].name != "Bill" || keys[1].name != "Bob" || keys[2].name != "John" {
		t.Errorf("expected names to be Bill, Bob, John, got %+v", keys)
	}
}

func TestFuncValues(t *testing.T) {
	m := map[int]func() int{
		7: func() int { return 3 },
		9: func() int { return 3 },
	}
	values := Values(m)
	if len(values) != 2 || values[1]() != 3 {
		t.Errorf("expected list of 2 functions got %+v", values)
	}
}

func TestEmptyKeysValues(t *testing.T) {
	emptyMap := map[string]int{}
	emptyKeys := Keys(emptyMap)
	if len(emptyKeys) != 0 {
		t.Errorf("expected empty list of keys for empty map, got %+v", emptyKeys)
	}
	emptyValues := Values(emptyMap)
	if len(emptyValues) != 0 {
		t.Errorf("expected empty list of values for empty map, got %+v", emptyValues)
	}
	var nilMap map[string]int
	nilKeys := Keys(nilMap)
	if len(emptyKeys) != 0 {
		t.Errorf("expected empty list of keys for nil map, got %+v", nilKeys)
	}
	nilValues := Values(nilMap)
	if len(nilValues) != 0 {
		t.Errorf("expected empty list of values for nil map, got %+v", nilValues)
	}
}
